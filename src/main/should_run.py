from cryptobot_commons.retrievers import AllocationResultsRetriever
from cryptobot_commons.retrievers import StrategiesResultsRetriever
import datetime
import sys

max_adddate_allocations = None
max_adddate_strategies = None

allocation_results = AllocationResultsRetriever("LIMITED_ORDERED", limit=1).execute().results
if allocation_results:
    last_allocation_result = allocation_results[0]
    max_adddate_allocations = last_allocation_result.add_date

strategies_results = StrategiesResultsRetriever("LIMITED_ORDERED", limit=1).execute().results
if strategies_results:
    last_strategies_result = strategies_results[0]
    max_adddate_strategies = last_strategies_result.add_date

datetime.datetime.now()

if strategies_results and not allocation_results:
    print(str(datetime.datetime.now()) + " | Should the program run ? Answer: yes")
    sys.exit(0)
elif (strategies_results and allocation_results) and \
    (max_adddate_strategies > max_adddate_allocations):
    print(str(datetime.datetime.now()) + " | Should the program run ? Answer: yes")
    sys.exit(0)
else:
    print(str(datetime.datetime.now()) + " | Should the program run ? Answer: no")
    sys.exit(1)