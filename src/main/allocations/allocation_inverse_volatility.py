from decimal import Decimal, getcontext
from cryptobot_commons import AllocationResults
from cryptobot_commons import Signal
from allocations.allocation import Allocation
from allocations.allocation_input import AllocationInput
from utils import Utils


class AllocationInverseVolatility(Allocation):
    """ AllocationInverseVolatility is an allocation where we choose to allocate a proportionnal amount to the inverse volatility
    to each LONG currency """

    def __init__(self, allocation_input: AllocationInput, hyperparameter: dict):
        super().__init__("INVERSE_VOLATILITY", allocation_input)
        self.hyperparameter = hyperparameter

    def calibrate_allocation(self):
        # We don't need to calibrate this strategy
        pass

    def _calculate_allocation(self):
        nb_long = self._compute_nb_long_positions()

        if nb_long > 1:
            ratios = self.__compute_allocation_inverse_volatility()
            allocation_results_raw = self.__fill_allocation_results_raw(ratios)
        elif nb_long == 1:
            results_with_long_signal = self.__find_results_with_long_signal(
                self.allocation_input.strategies_results.results)
            long_currencies_names = self.__find_long_currencies_names_in_results(results_with_long_signal)
            allocation_results_raw = self.__fill_allocation_results_raw({long_currencies_names[0]: 1.0})
        else:
            allocation_results_raw = self.__fill_allocation_results_raw({"EUR": 1.0})
        return AllocationResults(allocation_results_raw)

    def _compute_nb_long_positions(self):
        nb_long = 0
        for strategy_result in self.allocation_input.strategies_results.results:
            if strategy_result.signal is Signal.LONG:
                nb_long += 1
        return float(nb_long)

    def __find_results_with_long_signal(self, results):
        results_with_long_signal = []

        for strategy_result in results:
            if strategy_result.signal is Signal.LONG:
                results_with_long_signal.append(strategy_result)
        return results_with_long_signal

    def __find_long_currencies_names_in_results(self, results_with_long_signal):
        currencies_names = []

        for result_with_long_signal in results_with_long_signal:
            currencies_names.append(Utils.get_currency_from_pair(result_with_long_signal.currency_pair.name))
        return currencies_names

    def __find_pairs_with_currency_names(self, pairs, currencies_names):
        long_pairs = []

        for crypto_currency_pair in pairs:
            if Utils.get_currency_from_pair(crypto_currency_pair.name) in currencies_names:
                long_pairs.append(crypto_currency_pair)
        return long_pairs

    def __compute_inverse_volatilities(self, long_pairs):
        inverse_volatilities = {}

        for crypto_currency_pair in long_pairs:
            volatility = float(crypto_currency_pair.close.volatility(self.hyperparameter["std_period"])[-1])
            if volatility == 0.0:
                inverse_volatilities[Utils.get_currency_from_pair(crypto_currency_pair.name)] = 0.0
            else:
                inverse_volatilities[Utils.get_currency_from_pair(crypto_currency_pair.name)] = 1 / volatility
        return inverse_volatilities

    def __compute_ratios(self, inverse_volatilities):
        ratios = {}
        inverse_volatilities_sum = sum(inverse_volatilities.values())

        for name in inverse_volatilities:
            getcontext().prec = 6
            ratios[name] = float(
                Decimal(inverse_volatilities[name]) / Decimal(inverse_volatilities_sum))
        return ratios

    def __compute_allocation_inverse_volatility(self):
        results_with_long_signal = self.__find_results_with_long_signal(self.allocation_input.strategies_results.results)
        long_currencies_names = self.__find_long_currencies_names_in_results(results_with_long_signal)
        long_pairs = self.__find_pairs_with_currency_names(self.allocation_input.crypto_currency_pairs.pairs,
                                                          long_currencies_names)
        inverse_volatilities = self.__compute_inverse_volatilities(long_pairs)
        inverse_volatilities = self.__handle_non_volatile_currencies(inverse_volatilities)

        ratios = self.__compute_ratios(inverse_volatilities)
        return ratios

    def __fill_allocation_results_raw(self, ratios):
        allocation_results_raw = self._generate_allocation_results_raw()
        long_currencies_names = list(ratios.keys())

        for allocation_result_raw in allocation_results_raw:
            if allocation_result_raw["currency"] in long_currencies_names:
                allocation_result_raw["value_percent"] = ratios[allocation_result_raw["currency"]]
        return allocation_results_raw

    def __handle_non_volatile_currencies(self, inverse_volatilities: dict) -> dict:
        sum_inverse_volatilities = sum(inverse_volatilities.values())
        self.__error_if_all_currencies_are_non_volatile(sum_inverse_volatilities)

        for key in inverse_volatilities:
            if inverse_volatilities[key] == 0.0:
                inverse_volatilities[key] = sum_inverse_volatilities / len(inverse_volatilities)
        return inverse_volatilities

    def __error_if_all_currencies_are_non_volatile(self, sum_inverse_volatilities: float):
        if sum_inverse_volatilities == 0.0:
            raise Exception("Error: cannot compute Inverse Volatility allocation when all LONG "
                            "currencies are non volatile.")
