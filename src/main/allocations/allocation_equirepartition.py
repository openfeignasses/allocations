from decimal import Decimal, getcontext
from cryptobot_commons import AllocationResults
from cryptobot_commons import Signal
from allocations.allocation import Allocation
from allocations.allocation_simple_input import AllocationSimpleInput
from utils import Utils


class AllocationEquirepartition(Allocation):
    """ AllocationEquirepartition is an allocation where we choose to allocate the same amount
    to each LONG currency """

    def __init__(self, allocation_simple_input: AllocationSimpleInput):
        super().__init__("EQUIREPARTITION", allocation_simple_input)

    def calibrate_allocation(self):
        # We don't need to calibrate this strategy
        pass

    def _calculate_allocation(self):
        allocation_results_raw = self._generate_allocation_results_raw()
        # Compute how many LONG we have
        nbr_long = self.__compute_nb_long_positions()
        # Euro allocation
        if nbr_long == 0:  # First case, we have no LONG, so we only keep Euro in our portfolio
            for allocation_result_raw in allocation_results_raw:
                if allocation_result_raw["currency"] == "EUR":
                    allocation_result_raw["value_percent"] = float(1)
        # Cryptocurrencies allocation
        for strategy_result in self.allocation_input.strategies_results.results:
            if strategy_result.signal is Signal.LONG:
                for allocation_result_raw in allocation_results_raw:
                    if allocation_result_raw["currency"] == Utils.get_currency_from_pair(strategy_result.currency_pair.name):
                        getcontext().prec = 6
                        allocation_result_raw["value_percent"] = float(
                            Decimal(1) / Decimal(nbr_long))
        return AllocationResults(allocation_results_raw)

    def __compute_nb_long_positions(self):
        nbr_long = 0
        for strategy_result in self.allocation_input.strategies_results.results:
            if strategy_result.signal is Signal.LONG:
                nbr_long += 1
        return float(nbr_long)
