from cryptobot_commons import CryptoCurrencyPairs
from cryptobot_commons import StrategiesResults


class AllocationInput:
    def __init__(self,
                 strategies_results: StrategiesResults,
                 crypto_currency_pairs: CryptoCurrencyPairs):
        self.strategies_results = strategies_results
        self.crypto_currency_pairs = crypto_currency_pairs
        self.__check_initialized_attributes()

    def __check_initialized_attributes(self):
        if not isinstance(self.strategies_results, StrategiesResults):
            raise (Exception("The strategies_results attribute must be of type StrategiesResults."))
        if not isinstance(self.crypto_currency_pairs, CryptoCurrencyPairs):
            raise (Exception("The crypto_currency_pairs attribute must be of type CryptoCurrencyPairs."))
