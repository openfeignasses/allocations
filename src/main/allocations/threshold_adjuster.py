from cryptobot_commons import AllocationResult
from cryptobot_commons import AllocationResults
import os


class ThresholdAdjuster:

    MAX_ALLOCATION_DEFAULT = 0.6
    MIN_ALLOCATION_DEFAULT = 0.03
    INACCURACY_MARGIN_DEFAULT = 0.05

    def __init__(self, allocation_results: AllocationResults):
        self.allocation_results = allocation_results
        self.allocation_surplus = None
        self.allocation_lack = None
        
        self.max_allocation = None
        self.min_allocation = None
        self.inacuracy_margin = None
        self.__set_config()

    def __set_config(self):
        if os.getenv("MAX_ALLOCATION", None) is not None:
            self.max_allocation = float(os.getenv("MAX_ALLOCATION"))
        else:
            self.max_allocation = ThresholdAdjuster.MAX_ALLOCATION_DEFAULT

        if os.getenv("MIN_ALLOCATION", None) is not None:
            self.min_allocation = float(os.getenv("MIN_ALLOCATION"))
        else:
            self.min_allocation = ThresholdAdjuster.MIN_ALLOCATION_DEFAULT
        
        if os.getenv("INACCURACY_MARGIN", None) is not None:
            self.inacuracy_margin = float(os.getenv("INACCURACY_MARGIN"))
        else:
            self.inacuracy_margin = ThresholdAdjuster.INACCURACY_MARGIN_DEFAULT

    def adjust(self):

        self.__calculate_allocation_surplus()
        self.__calculate_allocation_lack()
        self.__compute_allocation_reduction()

        if self.__surplus_is_enough_to_fill_lack():
            self.__adjust_allocations_to_min_value_if_needed()
            self.__update_eur_allocation()
        else:
            sum_allocation_before_normalization = self.__calculate_available_allocation_to_fill_lacks()
            self.__redistribute_available_allocations(sum_allocation_before_normalization)

        return self

    def __redistribute_available_allocations(self, sum_allocation_before_normalization):
        # ici commence l'étape "3." des notes
        self.allocation_lack -= self.allocation_surplus
        # Normalize if above min allocation + 0.05
        self.__adjust_and_normalize_allocations(self.allocation_lack, sum_allocation_before_normalization)

    def __calculate_available_allocation_to_fill_lacks(self):
        sum_allocation_before_normalization = 0.0  # proportional adjustment
        for result in self.allocation_results.results:
            if self.__allocation_can_be_diminished(result):
                sum_allocation_before_normalization += result.value_percent
        return sum_allocation_before_normalization

    def __adjust_and_normalize_allocations(self, sum_for_min_alloc: float, sum_allocation_before_normalization: float):
        for result in self.allocation_results.results:
            if self.__allocation_can_be_diminished(result):
                # (sum_for_min_alloc/sum_allocation_before_normalization) =
                # combien il y a de manque à gagner / ce quon a de dispo pour normaliser
                result.value_percent -= result.value_percent * (sum_for_min_alloc / sum_allocation_before_normalization)
            if self.__allocation_should_be_increased(result):  # No allocation means we are SHORT on the crypto
                result.value_percent = self.min_allocation

    def __adjust_allocations_to_min_value_if_needed(self):
        for result in self.allocation_results.results:
            if self.__allocation_should_be_increased(result):
                self.__update_allocation_surplus(result)
                self.__adjust_allocation_to_min_value(result)

    def __compute_allocation_reduction(self):
        for result in self.allocation_results.results:
            if self.__is_enough(result.value_percent) and self.__is_not_eur(result.currency):
                result.value_percent = self.max_allocation

    def __calculate_allocation_surplus(self):
        allocation_surplus = 0
        for result in self.allocation_results.results:
            if self.__is_enough(result.value_percent) and self.__is_not_eur(result.currency):
                allocation_surplus += result.value_percent - self.max_allocation
        self.allocation_surplus = allocation_surplus

    def __calculate_allocation_lack(self):
        allocation_lack = 0
        for result in self.allocation_results.results:
            if self.__allocation_should_be_increased(result): # No allocation means we are SHORT on the crypto
                allocation_lack += self.min_allocation - result.value_percent
        self.allocation_lack = allocation_lack

    def __update_eur_allocation(self):
        for result in self.allocation_results.results:
            if self.__is_eur(result.currency):
                result.value_percent += self.allocation_surplus
                break

    def __surplus_is_enough_to_fill_lack(self):
        return self.allocation_surplus > self.allocation_lack

    def __is_lacking(self, value_percent):
        return value_percent < self.min_allocation

    def __is_enough(self, value_percent):
        return value_percent > self.max_allocation

    def __is_not_eur(self, currency: str):
        return currency != 'EUR'

    def __is_eur(self, currency: str):
        return currency == 'EUR'

    def __is_not_short(self, value_percent: float):
        return value_percent != 0.0

    def __calculate_delta_to_fill_lack(self, value_percent: float):
        return self.min_allocation - value_percent

    def __update_allocation_surplus(self, result: AllocationResult):
        self.allocation_surplus -= self.__calculate_delta_to_fill_lack(result.value_percent)

    def __adjust_allocation_to_min_value(self, allocation_result: AllocationResult):
        allocation_result.value_percent = self.min_allocation

    def __allocation_should_be_increased(self, result: AllocationResult):
        return self.__is_lacking(result.value_percent) \
               and self.__is_not_eur(result.currency) \
               and self.__is_not_short(result.value_percent)

    def __allocation_can_be_diminished(self, result: AllocationResult):
        return result.value_percent > (self.min_allocation + self.inacuracy_margin) \
               and self.__is_not_eur(result.currency)
