from cryptobot_commons import AllocationResults


class NormalizationNearOne:
    def __init__(self, allocation_results: AllocationResults):
        self.allocation_results = allocation_results

    def normalize(self):
        self.__normalize_allocation_if_sum_near_one()
        return self

    def __normalize_allocation_if_sum_near_one(self):
        sum_value_percent = 0.0
        for result in self.allocation_results.results:
            sum_value_percent += result.value_percent
        if (sum_value_percent > 1.0 and sum_value_percent < 1.001) or (
                sum_value_percent > 0.999 and sum_value_percent < 1.0):
            self.__normalize_allocation(sum_value_percent)

    def __normalize_allocation(self, sum_value_percent: float):
        for result in self.allocation_results.results:
            result.value_percent = result.value_percent / sum_value_percent
