from abc import ABC, abstractmethod
from cryptobot_commons import AllocationResults
from cryptobot_commons import InvestmentUniverse
from cryptobot_commons.savers import AllocationResultsSaver
from allocations.allocation_adjustor import AllocationAdjustor
from allocations.allocation_input import AllocationInput
from allocations.allocation_simple_input import AllocationSimpleInput
from allocations.allocation_validity_checker import AllocationValidityChecker
from utils import Utils


class Allocation(ABC):
    """
    Allocation class is the mother class of every allocation class
    """
    def __init__(self, name: str, allocation_input: AllocationInput):
        self.name = name
        self.allocation_input = allocation_input
        self._check_initialized_attributes()

    def _check_initialized_attributes(self):
        if not isinstance(self.name, str):
            raise (Exception("The allocation name must be of type string."))
        if not (isinstance(self.allocation_input, AllocationInput) or
                isinstance(self.allocation_input, AllocationSimpleInput)):
            raise (Exception("The allocation_input must be of type AllocationInput or AllocationSimpleInput."))

    @abstractmethod
    def calibrate_allocation(self):
        pass

    @abstractmethod
    def _calculate_allocation(self):
        pass

    def calculate_and_save_allocation(self):
        allocation_results = self._calculate_allocation()
        adjusted_allocation_results = AllocationAdjustor(allocation_results).adjust().allocation_results
        AllocationValidityChecker(adjusted_allocation_results).check()
        self.__save_allocation(adjusted_allocation_results)

    def __save_allocation(self, allocation_results: AllocationResults):
        allocation_results_saver = AllocationResultsSaver(allocation_results)
        allocation_results_saver.save()

    def _generate_allocation_results_raw(self):
        allocation_results_raw = []
        # First we generate all the AllocationResults associated to the cryptocurrencies
        for currency_pair in InvestmentUniverse:
            allocation_results_raw.append({
                "currency": Utils.get_currency_from_pair(currency_pair.name),
                "allocation_name": self.name,
                "execution_id": None,
                "value_percent": float(0),
                "add_date": None
            })
        # Then we generate the AllocationResults associated to the EUR (which is not in allocation_input)
        allocation_results_raw.append({
            "currency": "EUR",
            "allocation_name": self.name,
            "execution_id": None,
            "value_percent": float(0),
            "add_date": None
        })
        return allocation_results_raw
