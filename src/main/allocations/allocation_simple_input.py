from cryptobot_commons import StrategiesResults


class AllocationSimpleInput:
    def __init__(self,
                 strategies_results: StrategiesResults):
        self.strategies_results = strategies_results
        self.__check_initialized_attributes()

    def __check_initialized_attributes(self):
        if not isinstance(self.strategies_results, StrategiesResults):
            raise (Exception("The strategies_results must be of type StrategiesResults."))
