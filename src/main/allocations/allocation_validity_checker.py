from cryptobot_commons import AllocationResults
from decimal import Decimal, getcontext


class AllocationValidityChecker:
    def __init__(self, allocation_results: AllocationResults):
        self.allocation_results = allocation_results

    def check(self):
        sum_value_percent = Decimal(0.0)
        getcontext().prec = 7
        for result in self.allocation_results.results:
            sum_value_percent += Decimal(result.value_percent)
        if float(sum_value_percent) > 1.0:
            raise (Exception("The sum of allocations is above 1.0."))
        elif float(sum_value_percent) < 0.9999:
            raise (Exception("The sum of allocations is below 0.9999."))

