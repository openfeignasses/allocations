from cryptobot_commons import AllocationResults
from allocations.threshold_adjuster import ThresholdAdjuster
from allocations.normalization_near_one import NormalizationNearOne


class AllocationAdjustor:

    def __init__(self, allocation_results: AllocationResults):
        self.allocation_results = allocation_results

    def adjust(self):
        self.__adjust_min_max()
        self.__adjust_near_one()
        return self

    def __adjust_min_max(self):
        threshold_adjuster = ThresholdAdjuster(self.allocation_results)
        self.allocation_results = threshold_adjuster.adjust().allocation_results

    def __adjust_near_one(self):
        normalization_near_one = NormalizationNearOne(self.allocation_results)
        self.allocation_results = normalization_near_one.normalize().allocation_results
