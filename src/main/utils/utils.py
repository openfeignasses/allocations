from cryptobot_commons import AllocationResult
from cryptobot_commons import AllocationResults


class Utils:
    @staticmethod
    def find_by_currency(allocation_results: AllocationResults, currency_searched: str) -> AllocationResult:
        for result in allocation_results.results:
            if result.currency == currency_searched:
                return result
        return None

    @staticmethod
    def get_currency_from_pair(pair: str):
        return pair[:3]
