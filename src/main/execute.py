from cryptobot_commons.retrievers import CryptoCurrencyPairsRetriever
from cryptobot_commons.retrievers import StrategiesResultsRetriever
from allocations.allocation_inverse_volatility import AllocationInverseVolatility
from allocations.allocation_input import AllocationInput

strategies_results = StrategiesResultsRetriever("LAST_EXECUTION").execute()
crypto_currency_pairs = CryptoCurrencyPairsRetriever("ALL", "").execute()

allocation_input = AllocationInput(strategies_results, crypto_currency_pairs)
allocation_inverse_volatility = \
    AllocationInverseVolatility(allocation_input, {'std_period': 3}).calculate_and_save_allocation()

print("Execution finished.")
