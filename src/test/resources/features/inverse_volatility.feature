@Allocations
@Allocations_InverseVolatility
Feature: Allocation of assets in percent with the InverseVolatility model

   As Jean-Charles, the Allocation Manager
   In order to ideally
   - maximize the profit of the wallet
   - minimize the risk level of the wallet
   I want to calculate an allocation of the wallet value in percent for each currency

   @Allocations_InverseVolatility_1
   Scenario: 2 LONG signals with period = 3
      Given that the last signal for "BTC_EUR" is "LONG"
      And that the last signal for "LTC_EUR" is "LONG"
      And the "BTC_EUR" stock quotation table
      | time  | close |
      |   1   |   1   |
      |   2   |   2   |
      |   3   |   3   |
      |   4   |   3   |
      |   5   |   4   |
      |   6   |   3   |
      |   7   |   2   |
      |   8   |   1   |
      |   9   |   2   |
      |  10   |   2   |
      |  11   |   1   |
      |  12   |   2   |
      |  13   |   1   |
      |  14   |   2   |
      |  15   |   5   |
      |  16   |   4   |
      |  17   |   6   |
      |  18   |   8   |
      |  19   |   9   |
      |  20   |  11   |
      And the "LTC_EUR" stock quotation table
      | time  | close |
      |   1   |   5   |
      |   2   |   4   |
      |   3   |   2   |
      |   4   |   2   |
      |   5   |   3   |
      |   6   |   5   |
      |   7   |   6   |
      |   8   |   2   |
      |   9   |   1   |
      |  10   |   1   |
      |  11   |   2   |
      |  12   |   3   |
      |  13   |   4   |
      |  14   |   4   |
      |  15   |   9   |
      |  16   |  12   |
      |  17   |  11   |
      |  18   |  10   |
      |  19   |   9   |
      |  20   |   8   |
      And that the std_period hyperparameter is "6"
      When the InverseVolatility computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is "0.48269747"
      And the percentage allocated to the "LTC" is "0.51730252"
      And the percentage allocated to the "EUR" is "0.0"

   @Allocations_InverseVolatility_2
   Scenario: 2 LONG signals with period = 7
      Given that the last signal for "BTC_EUR" is "LONG"
      And that the last signal for "LTC_EUR" is "LONG"
      And the "BTC_EUR" stock quotation table
      | time  | close |
      |   1   |   1   |
      |   2   |   2   |
      |   3   |   3   |
      |   4   |   3   |
      |   5   |   4   |
      |   6   |   3   |
      |   7   |   2   |
      |   8   |   1   |
      |   9   |   2   |
      |  10   |   2   |
      |  11   |   1   |
      |  12   |   2   |
      |  13   |   1   |
      |  14   |   2   |
      |  15   |   5   |
      |  16   |   4   |
      |  17   |   6   |
      |  18   |   8   |
      |  19   |   9   |
      |  20   |  11   |
      And the "LTC_EUR" stock quotation table
      | time  | close |
      |   1   |   5   |
      |   2   |   4   |
      |   3   |   2   |
      |   4   |   2   |
      |   5   |   3   |
      |   6   |   5   |
      |   7   |   6   |
      |   8   |   2   |
      |   9   |   1   |
      |  10   |   1   |
      |  11   |   2   |
      |  12   |   3   |
      |  13   |   4   |
      |  14   |   4   |
      |  15   |   9   |
      |  16   |  12   |
      |  17   |  11   |
      |  18   |  10   |
      |  19   |   9   |
      |  20   |   8   |
      And that the std_period hyperparameter is "7"
      When the InverseVolatility computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is "0.46551445"
      And the percentage allocated to the "LTC" is "0.53448554"
      And the percentage allocated to the "EUR" is "0.0"

   @Allocations_InverseVolatility_3
   Scenario: 2 SHORT signals, 2 LONG signals
      Given that the last signal for "XLM_EUR" is "SHORT"
      And that the last signal for "GNO_EUR" is "SHORT"
      And that the last signal for "BTC_EUR" is "LONG"
      And that the last signal for "LTC_EUR" is "LONG"
      And the "BTC_EUR" stock quotation table
      | time  | close |
      |   1   |   1   |
      |   2   |   2   |
      |   3   |   3   |
      |   4   |   3   |
      |   5   |   4   |
      |   6   |   3   |
      |   7   |   2   |
      |   8   |   1   |
      |   9   |   2   |
      |  10   |   2   |
      |  11   |   1   |
      |  12   |   2   |
      |  13   |   1   |
      |  14   |   2   |
      |  15   |   5   |
      |  16   |   4   |
      |  17   |   6   |
      |  18   |   8   |
      |  19   |   9   |
      |  20   |  11   |
      And the "LTC_EUR" stock quotation table
      | time  | close |
      |   1   |   5   |
      |   2   |   4   |
      |   3   |   2   |
      |   4   |   2   |
      |   5   |   3   |
      |   6   |   5   |
      |   7   |   6   |
      |   8   |   2   |
      |   9   |   1   |
      |  10   |   1   |
      |  11   |   2   |
      |  12   |   3   |
      |  13   |   4   |
      |  14   |   4   |
      |  15   |   9   |
      |  16   |  12   |
      |  17   |  11   |
      |  18   |  10   |
      |  19   |   9   |
      |  20   |   8   |
      And the "XLM_EUR" stock quotation table
      | time  | close |
      |   1   |   7   |
      |   2   |   7   |
      |   3   |   6   |
      |   4   |   5   |
      |   5   |   5   |
      |   6   |   5   |
      |   7   |   3   |
      |   8   |   4   |
      |   9   |   4   |
      |  10   |   3   |
      |  11   |   2   |
      |  12   |   4   |
      |  13   |   2   |
      |  14   |   3   |
      |  15	  |   5   |
      |  16   |   7   |
      |  17   |   6   |
      |  18   |   7   |
      |  19   |   8   |
      |  20   |   9   |
      And the "GNO_EUR" stock quotation table
      | time  | close |
      |   1   |   9   |
      |   2   |   7   |
      |   3   |   6   |
      |   4   |   3   |
      |   5   |   2   |
      |   6   |   3   |
      |   7   |   4   |
      |   8   |   3   |
      |   9   |   1   |
      |  10   |   1   |
      |  11   |   2   |
      |  12   |   1   |
      |  13   |   2   |
      |  14   |   3   |
      |  15	  |   3   |
      |  16   |   2   |
      |  17   |   3   |
      |  18   |   3   |
      |  19   |   2   |
      |  20   |   1   |
      And that the std_period hyperparameter is "7"
      When the InverseVolatility computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "XLM" is "0.0"
      And the percentage allocated to the "GNO" is "0.0"
      And the percentage allocated to the "BTC" is "0.46551445"
      And the percentage allocated to the "LTC" is "0.53448554"

      And the percentage allocated to the "EUR" is "0.0"

   @Allocations_InverseVolatility_4
   Scenario: 3 LONG signals. Allocations values are rounded.
      Given that the last signal for "BTC_EUR" is "LONG"
      And that the last signal for "LTC_EUR" is "LONG"
      And that the last signal for "GNO_EUR" is "LONG"
      And the "BTC_EUR" stock quotation table
      | time  | close |
      |   1   |   1   |
      |   2   |   2   |
      |   3   |   3   |
      |   4   |   3   |
      |   5   |   4   |
      |   6   |   3   |
      |   7   |   2   |
      |   8   |   1   |
      |   9   |   2   |
      |  10   |   2   |
      |  11   |   1   |
      |  12   |   2   |
      |  13   |   1   |
      |  14   |   2   |
      |  15	  |   5   |
      |  16   |   4   |
      |  17   |   6   |
      |  18   |   8   |
      |  19   |   9   |
      |  20   |  11   |
      And the "LTC_EUR" stock quotation table
      | time  | close |
      |   1   |   5   |
      |   2   |   4   |
      |   3   |   2   |
      |   4   |   2   |
      |   5   |   3   |
      |   6   |   5   |
      |   7   |   6   |
      |   8   |   2   |
      |   9   |   1   |
      |  10   |   1   |
      |  11   |   2   |
      |  12   |   3   |
      |  13   |   4   |
      |  14   |   4   |
      |  15	  |   9   |
      |  16   |  12   |
      |  17   |  11   |
      |  18   |  10   |
      |  19   |   9   |
      |  20   |   8   |
      And the "GNO_EUR" stock quotation table
      | time  | close |
      |   1   |   9   |
      |   2   |   7   |
      |   3   |   6   |
      |   4   |   3   |
      |   5   |   2   |
      |   6   |   3   |
      |   7   |   4   |
      |   8   |   3   |
      |   9   |   1   |
      |  10   |   1   |
      |  11   |   2   |
      |  12   |   1   |
      |  13   |   2   |
      |  14   |   3   |
      |  15	  |   3   |
      |  16   |   2   |
      |  17   |   3   |
      |  18   |   3   |
      |  19   |   2   |
      |  20   |   1   |
      And that the std_period hyperparameter is "7"
      When the InverseVolatility computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is "0.27931748"
      And the percentage allocated to the "LTC" is "0.32070144"
      And the percentage allocated to the "GNO" is "0.39998107"
      And the percentage allocated to the "EUR" is "0.0"
