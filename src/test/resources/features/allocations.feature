@Allocations
@Allocations_Commons
Feature: Allocations are using a common code base

   As Jean-Charles, the Allocation Manager
   In order to be able to use all allocation models in the same way although their internal functioning is different
   I want all allocation models to use the same code base.
   This code base defines what are the possible inputs and outputs for all allocation models and the functional logic of an allocation model.

   Background: hyperparameters
      Given that the std_period hyperparameter is "3"
      And the "BTC_EUR" stock quotation chart
      """
      +---------------------------------------------+
      |                                            X|
      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX |
      +---------------------------------------------+
      """
      And the "LTC_EUR" stock quotation chart
      """
      +---------------------------------------------+
      |                                            X|
      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX |
      +---------------------------------------------+
      """
      And the "ETH_EUR" stock quotation chart
      """
      +---------------------------------------------+
      |                                             |
      |XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|
      +---------------------------------------------+
      """

   @Allocations_Commons_1
   Scenario Outline: Output is of type AllocationResults
      Given that the last signal for "BTC_EUR" is "LONG"
      And that the last signal for "LTC_EUR" is "LONG"
      And that the last signal for "ETH_EUR" is "SHORT"
      When the <Allocaton Model> computes the optimal allocation with optimal hyperparameters
      Then the output type is "AllocationResults"
      Examples:
      |    Allocaton Model         |
      |    Equirepartition         |
      |    InverseVolatility       |

   @Allocations_Commons_2
   Scenario Outline: 1 LONG signal
      Given that the last signal for "BTC_EUR" is "LONG"
      When the <Allocaton Model> computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is "0.6"
      And the percentage allocated to the "EUR" is "0.4"
      Examples:
      |    Allocaton Model         |
      |    Equirepartition         |
      |    InverseVolatility       |

   @Allocations_Commons_3
   Scenario Outline: 1 SHORT signal
      Given that the last signal for "BTC_EUR" is "SHORT"
      When the <Allocaton Model> computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is "0.0"
      And the percentage allocated to the "EUR" is "1.0"
      Examples:
      |    Allocaton Model         |
      |    Equirepartition         |
      |    InverseVolatility       |

   @Allocations_Commons_4
   Scenario Outline: 1 NEUTRAL signal
      Given that the last signal for "BTC_EUR" is "NEUTRAL"
      When the <Allocaton Model> computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is "0.0"
      And the percentage allocated to the "EUR" is "1.0"
      Examples:
      |    Allocaton Model         |
      |    Equirepartition         |
      |    InverseVolatility       |

   @Allocations_Commons_5
   Scenario Outline: 2 SHORT signals, 1 LONG
      Given that the last signal for "BTC_EUR" is "SHORT"
      And that the last signal for "LTC_EUR" is "LONG"
      And that the last signal for "ETH_EUR" is "SHORT"
      When the <Allocaton Model> computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is "0.0"
      And the percentage allocated to the "LTC" is "0.6"
      And the percentage allocated to the "ETH" is "0.0"
      And the percentage allocated to the "EUR" is "0.4"
      Examples:
      |    Allocaton Model         |
      |    Equirepartition         |
      |    InverseVolatility       |

   @Allocations_Commons_6
   Scenario Outline: 2 NEUTRAL signals, 1 LONG
      Given that the last signal for "BTC_EUR" is "NEUTRAL"
      And that the last signal for "LTC_EUR" is "LONG"
      And that the last signal for "ETH_EUR" is "NEUTRAL"
      When the <Allocaton Model> computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is "0.0"
      And the percentage allocated to the "LTC" is "0.6"
      And the percentage allocated to the "ETH" is "0.0"
      And the percentage allocated to the "EUR" is "0.4"
      Examples:
      |    Allocaton Model         |
      |    Equirepartition         |
      |    InverseVolatility       |

   @Allocations_Commons_7
   Scenario Outline: Allocation is not null with LONG signal
      Given that the last signal for "BTC_EUR" is "LONG"
      And that the last signal for "LTC_EUR" is "SHORT"
      And that the last signal for "ETH_EUR" is "LONG"
      When the <Allocaton Model> computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is above "0.0"
      And the percentage allocated to the "ETH" is above "0.0"
      Examples:
      |    Allocaton Model         |
      |    Equirepartition         |
      |    InverseVolatility       |

   @Allocations_Commons_8
   Scenario Outline: Sum of allocations is 1.0 rounded
      Given that the last signal for "BTC_EUR" is "SHORT"
      And that the last signal for "LTC_EUR" is "LONG"
      And that the last signal for "ETH_EUR" is "LONG"
      When the <Allocaton Model> computes the optimal allocation with optimal hyperparameters
      Then the sum of the percentages allocated is equal to "1.0" rounded
      Examples:
      |    Allocaton Model         |
      |    Equirepartition         |
      |    InverseVolatility       |

   @Allocations_Commons_9
   Scenario Outline: Each allocation minimum is 0.0, maximum is 1.0
      Given that the last signal for "BTC_EUR" is "SHORT"
      And that the last signal for "LTC_EUR" is "LONG"
      And that the last signal for "ETH_EUR" is "SHORT"
      When the <Allocaton Model> computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is between "0.0" and "1.0"
      And the percentage allocated to the "LTC" is between "0.0" and "1.0"
      And the percentage allocated to the "ETH" is between "0.0" and "1.0"
      Examples:
      |    Allocaton Model         |
      |    Equirepartition         |
      |    InverseVolatility       |

   @Allocations_Commons_10
   Scenario Outline: 3 SHORT signals
      Given that the last signal for "BTC_EUR" is "SHORT"
      And that the last signal for "LTC_EUR" is "SHORT"
      And that the last signal for "ETH_EUR" is "SHORT"
      When the <Allocaton Model> computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is "0.0"
      And the percentage allocated to the "LTC" is "0.0"
      And the percentage allocated to the "ETH" is "0.0"
      And the percentage allocated to the "EUR" is "1.0"
      Examples:
      |    Allocaton Model         |
      |    Equirepartition         |
      |    InverseVolatility       |

   @Allocations_Commons_11
   Scenario Outline: 3 NEUTRAL signals
      Given that the last signal for "BTC_EUR" is "NEUTRAL"
      And that the last signal for "LTC_EUR" is "NEUTRAL"
      And that the last signal for "ETH_EUR" is "NEUTRAL"
      When the <Allocaton Model> computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is "0.0"
      And the percentage allocated to the "LTC" is "0.0"
      And the percentage allocated to the "ETH" is "0.0"
      And the percentage allocated to the "EUR" is "1.0"
      Examples:
      |    Allocaton Model         |
      |    Equirepartition         |
      |    InverseVolatility       |

   @Allocations_Commons_12
   Scenario Outline: 3 LONG signals
      Given that the last signal for "BTC_EUR" is "LONG"
      And that the last signal for "LTC_EUR" is "LONG"
      And that the last signal for "ETH_EUR" is "LONG"
      When the <Allocaton Model> computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is above "0.0"
      And the percentage allocated to the "LTC" is above "0.0"
      And the percentage allocated to the "ETH" is above "0.0"
      Examples:
      |    Allocaton Model         |
      |    Equirepartition         |
      |    InverseVolatility       |

   @Allocations_Commons_13
   Scenario Outline: All the allocation's value are expected to be float
      Given that the last signal for "BTC_EUR" is "LONG"
      And that the last signal for "LTC_EUR" is "SHORT"
      And that the last signal for "ETH_EUR" is "NEUTRAL"
      When the <Allocaton Model> computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is a float
      And the percentage allocated to the "LTC" is a float
      And the percentage allocated to the "ETH" is a float
      And the percentage allocated to the "EUR" is a float
      Examples:
      |    Allocaton Model         |
      |    Equirepartition         |
      |    InverseVolatility       |
