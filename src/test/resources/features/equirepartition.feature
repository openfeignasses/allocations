@Allocations
@Allocations_Equirepartition
Feature: Allocation of assets in percent with the Equirepartition model

   As Jean-Charles, the Allocation Manager
   In order to ideally
   - maximize the profit of the wallet
   - minimize the risk level of the wallet
   I want to calculate an allocation of the wallet value in percent for each currency

   @Allocations_Equirepartition_1
   Scenario: 2 LONG signals
      Given that the last signal for "BTC_EUR" is "LONG"
      And that the last signal for "LTC_EUR" is "LONG"
      When the Equirepartition computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is "0.5"
      And the percentage allocated to the "LTC" is "0.5"
      And the percentage allocated to the "EUR" is "0.0"

   @Allocations_Equirepartition_2
   Scenario: 2 SHORT signals, 2 LONG signals
      Given that the last signal for "XLM_EUR" is "SHORT"
      And that the last signal for "GNO_EUR" is "SHORT"
      And that the last signal for "BTC_EUR" is "LONG"
      And that the last signal for "LTC_EUR" is "LONG"
      When the Equirepartition computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "XLM" is "0.0"
      And the percentage allocated to the "GNO" is "0.0"
      And the percentage allocated to the "BTC" is "0.5"
      And the percentage allocated to the "LTC" is "0.5"
      And the percentage allocated to the "EUR" is "0.0"

   @Allocations_Equirepartition_3
   Scenario: 3 LONG signals. Allocations values are rounded.
      Given that the last signal for "BTC_EUR" is "LONG"
      And that the last signal for "LTC_EUR" is "LONG"
      And that the last signal for "GNO_EUR" is "LONG"
      When the Equirepartition computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is "0.33333333"
      And the percentage allocated to the "LTC" is "0.33333333"
      And the percentage allocated to the "GNO" is "0.33333333"
      And the percentage allocated to the "EUR" is "0.0"
      And the sum of the percentages allocated is equal to "1.0" rounded

   @Allocations_Equirepartition_4
   Scenario: 9 LONG signals. Allocations values are rounded.
      Given that the last signal for "BTC_EUR" is "LONG"
      And that the last signal for "GNO_EUR" is "LONG"
      And that the last signal for "ETH_EUR" is "LONG"
      And that the last signal for "ETC_EUR" is "LONG"
      And that the last signal for "REP_EUR" is "LONG"
      And that the last signal for "XLM_EUR" is "LONG"
      And that the last signal for "EOS_EUR" is "LONG"
      And that the last signal for "ZEC_EUR" is "LONG"
      And that the last signal for "XMR_EUR" is "LONG"
      When the Equirepartition computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is "0.11111111"
      And the percentage allocated to the "GNO" is "0.11111111"
      And the percentage allocated to the "ETH" is "0.11111111"
      And the percentage allocated to the "ETC" is "0.11111111"
      And the percentage allocated to the "REP" is "0.11111111"
      And the percentage allocated to the "XLM" is "0.11111111"
      And the percentage allocated to the "EOS" is "0.11111111"
      And the percentage allocated to the "ZEC" is "0.11111111"
      And the percentage allocated to the "XMR" is "0.11111111"
      And the percentage allocated to the "EUR" is "0.0"
      And the sum of the percentages allocated is equal to "1.0" rounded

   @Allocations_Equirepartition_5
   Scenario: 11 LONG signals. Allocations values are rounded.
      Given that the last signal for "BTC_EUR" is "LONG"
      And that the last signal for "GNO_EUR" is "LONG"
      And that the last signal for "ETH_EUR" is "LONG"
      And that the last signal for "ETC_EUR" is "LONG"
      And that the last signal for "REP_EUR" is "LONG"
      And that the last signal for "XLM_EUR" is "LONG"
      And that the last signal for "EOS_EUR" is "LONG"
      And that the last signal for "ZEC_EUR" is "LONG"
      And that the last signal for "XMR_EUR" is "LONG"
      And that the last signal for "XRP_EUR" is "LONG"
      And that the last signal for "LTC_EUR" is "LONG"
      When the Equirepartition computes the optimal allocation with optimal hyperparameters
      Then the percentage allocated to the "BTC" is "0.09090909"
      And the percentage allocated to the "GNO" is "0.09090909"
      And the percentage allocated to the "ETH" is "0.09090909"
      And the percentage allocated to the "ETC" is "0.09090909"
      And the percentage allocated to the "REP" is "0.09090909"
      And the percentage allocated to the "XLM" is "0.09090909"
      And the percentage allocated to the "EOS" is "0.09090909"
      And the percentage allocated to the "ZEC" is "0.09090909"
      And the percentage allocated to the "XMR" is "0.09090909"
      And the percentage allocated to the "XRP" is "0.09090909"
      And the percentage allocated to the "LTC" is "0.09090909"
      And the percentage allocated to the "EUR" is "0.0"
      And the sum of the percentages allocated is equal to "1.0" rounded