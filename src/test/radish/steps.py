from radish import given, when, then, before
from decimal import Decimal, getcontext
from allocations.allocation_simple_input import AllocationSimpleInput
from allocations.allocation_input import AllocationInput
from allocations.allocation_equirepartition import AllocationEquirepartition
from allocations.allocation_inverse_volatility import AllocationInverseVolatility
from cryptobot_commons import Signal
from cryptobot_commons import InvestmentUniverse
from cryptobot_commons.retrievers import AllocationResultsRetriever
from cryptobot_commons.retrievers import StrategiesResultsRetriever
from cryptobot_commons.savers import StrategiesResultsSaver
from cryptobot_commons import AllocationResults
from cryptobot_commons import StrategiesResults
from cryptobot_commons.repositories import StrategiesResultsRepository
from cryptobot_commons.repositories import AllocationsResultsRepository
from cryptobot_commons import CryptoCurrencyPairBuilder
from cryptobot_commons import CryptoCurrencyPairs
from cryptobot_commons.utils import TextChartConverter
from utils import Utils


def convert_time_to_int(table_to_format:list):
    for line in table_to_format:
        line["time"] = int(line["time"])
    return table_to_format


def fill_open_high_low_vwap_volume_count(stock_quotations_for_currency_raw):
    for line in stock_quotations_for_currency_raw:
        line["open"] = 0
        line["high"] = 0
        line["low"] = 0
        line["vwap"] = 0
        line["volume"] = 0
        line["count"] = 0
    return stock_quotations_for_currency_raw


@before.each_scenario
def init_dicts(scenario):
    scenario.context.crypto_currency_pairs = {}


@before.each_scenario
def clean_tables(scenario):
    scenario.context.strategies_results_repository = StrategiesResultsRepository()
    scenario.context.allocations_results_repository = AllocationsResultsRepository()
    scenario.context.strategies_results_repository.truncate()
    scenario.context.allocations_results_repository.truncate()


@before.each_scenario
def clean_signals(scenario):
    scenario.context.signals = {}


@given('that the last signal for "{pair:w}" is "{signal:w}"')
def my_step(step, pair, signal):
    signal_enum = Signal[signal]
    step.context.signals[Utils.get_currency_from_pair(pair)] = signal_enum

    StrategiesResultsSaver(StrategiesResults([{
        "strategy_name": "SMA_Crossovers",
        "currency_pair": InvestmentUniverse[pair],
        "signal": signal_enum,
        "add_date": None,
        "execution_id": 1
    }])).save()


@given('that no last signal for "{pair:w}" is available')
def my_step(step, pair):
    pass


@given('the "{pair:w}" stock quotation table')
def step_impl(step, pair):
    formatted_table = convert_time_to_int(step.table)
    formatted_table = fill_open_high_low_vwap_volume_count(formatted_table)
    step.context.crypto_currency_pairs[pair] = CryptoCurrencyPairBuilder(pair, formatted_table).build()


@given('the "{pair:w}" stock quotation chart')
def step_impl(step, pair):
    stock_quotations_for_currency_raw = TextChartConverter(step.text).to_dicts()
    stock_quotations_for_currency_raw = fill_open_high_low_vwap_volume_count(stock_quotations_for_currency_raw)
    step.context.crypto_currency_pairs[pair] = CryptoCurrencyPairBuilder(
        pair,
        stock_quotations_for_currency_raw
    ).build()


@given('that the std_period hyperparameter is "{std_period_value:g}"')
def my_step(step, std_period_value):
    step.context.hyperparameters = {"std_period": int(std_period_value)}


@when('the Equirepartition computes the optimal allocation with optimal hyperparameters')
def my_step(step):
    strategies_results_retriever = StrategiesResultsRetriever("LAST_EXECUTION")
    strategies_results = strategies_results_retriever.execute()
    allocation_simple_input = AllocationSimpleInput(strategies_results)
    # Equirepartition
    our_allocation = AllocationEquirepartition(allocation_simple_input)
    our_allocation.calculate_and_save_allocation()


@when('the InverseVolatility computes the optimal allocation with optimal hyperparameters')
def my_step(step):
    strategies_results_retriever = StrategiesResultsRetriever("LAST_EXECUTION")
    strategies_results = strategies_results_retriever.execute()
    crypto_currency_pairs = CryptoCurrencyPairs([step.context.crypto_currency_pairs[pair] for pair in step.context.crypto_currency_pairs])
    allocation_input = AllocationInput(strategies_results, crypto_currency_pairs)
    # Inverse Volatility
    our_allocation = AllocationInverseVolatility(allocation_input, step.context.hyperparameters)
    our_allocation.calculate_and_save_allocation()


@then('the percentage allocated to the "{pair:w}" is "{expected_allocation:g}"')
def my_step(step, pair, expected_allocation):
    allocation_result = Utils.find_by_currency(AllocationResultsRetriever("LAST_EXECUTION").execute(), pair)
    getcontext().prec = 6
    assert Decimal(allocation_result.value_percent)/Decimal(1) == Decimal(expected_allocation)/Decimal(1)


@then('the percentage allocated to the "{pair:w}" is above "{minimal_allocation:g}"')
def my_step(step, pair, minimal_allocation):
    allocation_result = Utils.find_by_currency(AllocationResultsRetriever("LAST_EXECUTION").execute(), pair)
    assert round(allocation_result.value_percent, 6) > round(minimal_allocation, 6)


@then('the sum of the percentages allocated is equal to "{expected_percentage:g}" rounded')
def my_step(step, expected_percentage):
    allocations_results = AllocationResultsRetriever("LAST_EXECUTION").execute()
    total = 0
    for result in allocations_results.results:
        total += result.value_percent
    assert round(total) == expected_percentage


@then(
    'the percentage allocated to the "{pair:w}" is between "{minimal_allocation:g}" and "{maximal_allocation:g}"')
def my_step(step, pair, minimal_allocation, maximal_allocation):
    allocation_result = Utils.find_by_currency(AllocationResultsRetriever("LAST_EXECUTION").execute(), pair)
    assert round(allocation_result.value_percent, 6) >= round(minimal_allocation, 6)
    assert round(allocation_result.value_percent, 6) <= round(maximal_allocation, 6)


@then('the percentage allocated to the "{pair:w}" is a float')
def my_step(step, pair):
    allocation_result = Utils.find_by_currency(AllocationResultsRetriever("LAST_EXECUTION").execute(), pair)
    assert type(allocation_result.value_percent) == float


@then('the output type is "AllocationResults"')
def my_step(step):
    allocation_results = AllocationResultsRetriever("LAST_EXECUTION").execute()
    assert type(allocation_results) == AllocationResults
