import unittest
from cryptobot_commons.allocation_results import AllocationResults
from allocations.threshold_adjuster import ThresholdAdjuster
from unittest.mock import patch

class ThresholdAdjusterConfigTestCase(unittest.TestCase):

    @patch.dict('os.environ', {
        'MAX_ALLOCATION': "0.5",
    })
    def test_override_config_1(self):
        """ Check that config can be overriden
            by environment variables
        """
        allocation_results = AllocationResults([{
            "currency": "EUR",
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0),
            "add_date": None
        }])
        threshold_adjuster = ThresholdAdjuster(allocation_results)
        assert threshold_adjuster.max_allocation == 0.5

    @patch.dict('os.environ', {
        'MIN_ALLOCATION': "0.02",
    })
    def test_override_config_2(self):
        """ Check that config can be overriden
            by environment variables
        """
        allocation_results = AllocationResults([{
            "currency": "EUR",
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0),
            "add_date": None
        }])
        threshold_adjuster = ThresholdAdjuster(allocation_results)
        assert threshold_adjuster.min_allocation == 0.02

    @patch.dict('os.environ', {
        'INACCURACY_MARGIN': "0.06",
    })
    def test_override_config_3(self):
        """ Check that config can be overriden
            by environment variables
        """
        allocation_results = AllocationResults([{
            "currency": "EUR",
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0),
            "add_date": None
        }])
        threshold_adjuster = ThresholdAdjuster(allocation_results)
        assert threshold_adjuster.inacuracy_margin == 0.06
