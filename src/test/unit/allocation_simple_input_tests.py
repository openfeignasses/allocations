import unittest
from cryptobot_commons import InvestmentUniverse
from cryptobot_commons import Signal
from cryptobot_commons import InvestmentUniverseStatus
from cryptobot_commons import StrategiesResults
from allocations.allocation_simple_input import AllocationSimpleInput


class AllocationSimpleInputTestCase(unittest.TestCase):

    def setUp(cls):
        cls.strategies_results = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1589113875,
            "execution_id": 1
        }, {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.LTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1589113875,
            "execution_id": 1
        }])
        
    def test_instanciate_class(self):
        AllocationSimpleInput(
            strategies_results=self.strategies_results)

    def test_wrong_constructor_parameter(self):
        with self.assertRaises(Exception):
            AllocationSimpleInput({})
