import unittest
from cryptobot_commons.allocation_results import AllocationResults
from allocations.allocation_adjustor import AllocationAdjustor
from utils.utils import Utils


def truncate(f, n):
    '''Truncates/pads a float f to n decimal places without rounding'''
    s = '{}'.format(f)
    if 'e' in s or 'E' in s:
        return '{0:.{1}f}'.format(f, n)
    i, p, d = s.partition('.')
    return '.'.join([i, (d+'0'*n)[:n]])


class NormalizationNearOneTestCase(unittest.TestCase):

    def test_normalize_allocation_if_sum_near_one_1(self):
        # Generation of the allocation results
        allocation_results_raw = []
        allocation_results_raw.append({
            "currency": Utils.get_currency_from_pair("BTC_EUR"),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.5),
            "add_date": None
        })
        allocation_results_raw.append({
            "currency": Utils.get_currency_from_pair('LTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.4),
            "add_date": None
        })
        allocation_results_raw.append({
            "currency": Utils.get_currency_from_pair('ETH_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.1001),
            "add_date": None
        })
        allocation_results_raw.append({
            "currency": "EUR",
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0),
            "add_date": None
        })
        allocation_results = AllocationResults(allocation_results_raw)
        allocation_adjustor = AllocationAdjustor(allocation_results).adjust()
        allocation_results_new = allocation_adjustor.allocation_results
        sum_value_percent = 0.0
        for result in allocation_results_new.results:
            sum_value_percent += result.value_percent
        assert sum_value_percent==float(1.0)

    def test_normalize_allocation_if_sum_near_one_2(self):
        # Generation of the allocation results
        allocation_results_raw = []
        allocation_results_raw.append({
            "currency": Utils.get_currency_from_pair('BTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.5),
            "add_date": None
        })
        allocation_results_raw.append({
            "currency": Utils.get_currency_from_pair('LTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.4),
            "add_date": None
        })
        allocation_results_raw.append({
            "currency": Utils.get_currency_from_pair('ETH_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.0999),
            "add_date": None
        })
        allocation_results_raw.append({
            "currency": "EUR",
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0),
            "add_date": None
        })
        allocation_results = AllocationResults(allocation_results_raw)
        allocation_adjustor = AllocationAdjustor(allocation_results).adjust()
        allocation_results_new = allocation_adjustor.allocation_results
        sum_value_percent = 0.0
        for result in allocation_results_new.results:
            sum_value_percent += result.value_percent
        assert sum_value_percent==float(1.0)
