import unittest
from cryptobot_commons.allocation_results import AllocationResults
from allocations.allocation_adjustor import AllocationAdjustor
from utils.utils import Utils


def truncate(f, n):
    '''Truncates/pads a float f to n decimal places without rounding'''
    s = '{}'.format(f)
    if 'e' in s or 'E' in s:
        return '{0:.{1}f}'.format(f, n)
    i, p, d = s.partition('.')
    return '.'.join([i, (d+'0'*n)[:n]])


class ThresholdAdjusterTestCase(unittest.TestCase):

    def test_min_max_allocation_0(self):
        """ Happy path
        """
        # Generation of the allocation results
        allocation_results_raw_initial = []
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('BTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.6),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('LTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.3),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('ETH_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.05),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('GNO_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.05),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": "EUR",
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0),
            "add_date": None
        })
        allocation_results = AllocationResults(allocation_results_raw_initial)
        allocation_adjustor = AllocationAdjustor(allocation_results).adjust()
        allocation_results_new = allocation_adjustor.allocation_results
        expected_allocation={
            'BTC':0.6,
            'LTC':0.3,
            'ETH':0.05,
            'GNO':0.05,
            'EUR':0.0
        }
        for result in allocation_results_new.results:
            assert truncate(result.value_percent,6)==truncate(expected_allocation[result.currency], 6)

    def test_min_max_allocation_1(self):
        """ BTC allocation above the max allocation (0.6) 
        """
        # Generation of the allocation results
        allocation_results_raw_initial = []
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('BTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.8),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('LTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.1),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('ETH_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.05),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('GNO_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.05),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": "EUR",
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0),
            "add_date": None
        })
        allocation_results = AllocationResults(allocation_results_raw_initial)
        allocation_adjustor = AllocationAdjustor(allocation_results).adjust()
        allocation_results_new = allocation_adjustor.allocation_results
        
        expected_allocation={
            'BTC':0.6,
            'LTC':0.1,
            'ETH':0.05,
            'GNO':0.05,
            'EUR':0.2
        }

        for result in allocation_results_new.results:
            print("currency: "+result.currency)
            print("result.value_percent: "+str(result.value_percent))
            assert round(result.value_percent,6)==round(expected_allocation[result.currency], 6)

    def test_min_max_allocation_2(self):
        """ ETH and GNO allocations below the min allocation (0.03)
        """
        # Generation of the allocation results
        allocation_results_raw_initial = []
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('BTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.5),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('LTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.45),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('ETH_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.025),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('GNO_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.025),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": "EUR",
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0),
            "add_date": None
        })
        allocation_results = AllocationResults(allocation_results_raw_initial)
        allocation_adjustor = AllocationAdjustor(allocation_results).adjust()
        allocation_results_new = allocation_adjustor.allocation_results
        expected_allocation={
            'BTC':0.49473684,
            'LTC':0.44526316,
            'ETH':0.03,
            'GNO':0.03,
            'EUR':0.0
        }
        for result in allocation_results_new.results:
            assert truncate(result.value_percent,6)==truncate(expected_allocation[result.currency], 6)

    def test_min_max_allocation_3(self):
        """ ETH and GNO allocations below the min allocation (0.03)
        And the BTC allocation is above the max allocation (0.6)
        """
        # Generation of the allocation results
        allocation_results_raw_initial = []
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('BTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.8),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('LTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.15),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('ETH_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.025),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('GNO_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.025),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": "EUR",
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0),
            "add_date": None
        })
        allocation_results = AllocationResults(allocation_results_raw_initial)
        allocation_adjustor = AllocationAdjustor(allocation_results).adjust()
        allocation_results_new = allocation_adjustor.allocation_results
        expected_allocation={
            'BTC':0.6,
            'LTC':0.15,
            'ETH':0.03,
            'GNO':0.03,
            'EUR':0.19
        }
        for result in allocation_results_new.results:
            assert truncate(result.value_percent,6)==truncate(expected_allocation[result.currency], 6)

    def test_min_max_allocation_4(self):
        """ ETH allocation below the min allocation (0.03)
        BTC allocation above the max allocation (0.6)
        """
        # Generation of the allocation results
        allocation_results_raw_initial = []
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('BTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.8),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('LTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.175),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('ETH_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.025),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('GNO_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.0),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": "EUR",
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0),
            "add_date": None
        })
        allocation_results = AllocationResults(allocation_results_raw_initial)
        allocation_adjustor = AllocationAdjustor(allocation_results).adjust()
        allocation_results_new = allocation_adjustor.allocation_results
        expected_allocation={
            'BTC':0.6,
            'LTC':0.175,
            'ETH':0.03,
            'GNO':0.0,
            'EUR':0.195
        }
        for result in allocation_results_new.results:
            assert truncate(result.value_percent,6)==truncate(expected_allocation[result.currency], 6)

    def test_min_max_allocation_5(self):
        """ ETH allocation below the min allocation (0.03)
        """
        # Generation of the allocation results
        allocation_results_raw_initial = []
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('BTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.55),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('LTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.425),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('ETH_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.025),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('GNO_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.0),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": "EUR",
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0),
            "add_date": None
        })
        allocation_results = AllocationResults(allocation_results_raw_initial)
        allocation_adjustor = AllocationAdjustor(allocation_results).adjust()
        allocation_results_new = allocation_adjustor.allocation_results
        expected_allocation={
            'BTC':0.54717949,
            'LTC':0.42282051,
            'ETH':0.03,
            'GNO':0.00,
            'EUR':0.0
        }
        for result in allocation_results_new.results:
            assert truncate(result.value_percent,6)==truncate(expected_allocation[result.currency], 6)

    def test_min_max_allocation_6(self):
        """ ETH allocation below the min allocation (0.03)
        BTC above the max allocation (0.6)
        But the surplus is not enough for the ETH to be above the minimal_allocation (0.03)
        """
        # Generation of the allocation results
        allocation_results_raw_initial = []
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('BTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.604),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('LTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.371),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('ETH_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.025),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('GNO_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.0),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": "EUR",
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0),
            "add_date": None
        })
        allocation_results = AllocationResults(allocation_results_raw_initial)
        allocation_adjustor = AllocationAdjustor(allocation_results).adjust()
        allocation_results_new = allocation_adjustor.allocation_results
        expected_allocation={
            'BTC':0.59938208,
            'LTC':0.37061792,
            'ETH':0.03,
            'GNO':0.0,
            'EUR':0.0
        }
        for result in allocation_results_new.results:
            assert truncate(result.value_percent,6)==truncate(expected_allocation[result.currency], 6)

    def test_min_max_allocation_7(self):
        """ Extrem case where we have an allocation just above 0.03, and below 0.035
        When we are below 0.035 initially, it is expected to not normalize this allocation
        """
        # Generation of the allocation results
        allocation_results_raw_initial = []
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('BTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.5),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('LTC_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.459),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('ETH_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.01),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": Utils.get_currency_from_pair('GNO_EUR'),
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0.031),
            "add_date": None
        })
        allocation_results_raw_initial.append({
            "currency": "EUR",
            "allocation_name": 'EQUIREPARTITION',
            "execution_id": None,
            "value_percent": float(0),
            "add_date": None
        })
        allocation_results = AllocationResults(allocation_results_raw_initial)
        allocation_adjustor = AllocationAdjustor(allocation_results).adjust()
        allocation_results_new = allocation_adjustor.allocation_results
        expected_allocation={
            'BTC':0.48957247,
            'LTC':0.44942753,
            'ETH':0.03,
            'GNO':0.031,
            'EUR':0.0
        }
        for result in allocation_results_new.results:
            assert truncate(result.value_percent,6)==truncate(expected_allocation[result.currency], 6)