import unittest
from cryptobot_commons import CryptoCurrencyPairBuilder
from cryptobot_commons import CryptoCurrencyPairs
from cryptobot_commons import InvestmentUniverse
from cryptobot_commons import Signal
from cryptobot_commons import InvestmentUniverseStatus
from allocations.allocation_input import AllocationInput
from cryptobot_commons import StrategiesResults


class AllocationInputTestCase(unittest.TestCase):

    def setUp(cls):
        cls.strategies_results = StrategiesResults([{
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.BTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1589113875,
            "execution_id": 1
        }, {
            "strategy_name": "SMA_Crossovers",
            "currency_pair": InvestmentUniverse.LTC_EUR,
            "signal": Signal.LONG,
            "add_date": 1589113875,
            "execution_id": 1
        }])
        cls.crypto_currency_pairs = CryptoCurrencyPairs([
            CryptoCurrencyPairBuilder("BTC_EUR", [{
            "open":0.0,
            "high":0.0,
            "low":0.0,
            "close": 1601.00000000,
            "vwap":0.0,
            "volume":0.0,
            "count":0.0,
            "time": 1234568
        }, {
            "open":0.0,
            "high":0.0,
            "low":0.0,
            "close": 1602.00000000,
            "vwap":0.0,
            "volume":0.0,
            "count":0.0,
            "time": 1234569
        }]).build(),
        CryptoCurrencyPairBuilder("LTC_EUR", [{
            "open":0.0,
            "high":0.0,
            "low":0.0,
            "close": 1601.00000000,
            "vwap":0.0,
            "volume":0.0,
            "count":0.0,
            "time": 1234568
        }, {
            "open":0.0,
            "high":0.0,
            "low":0.0,
            "close": 1602.00000000,
            "vwap":0.0,
            "volume":0.0,
            "count":0.0,
            "time": 1234569
        }]).build()
        ])

    def test_instanciate_class(self):
        AllocationInput(
            strategies_results=self.strategies_results,
            crypto_currency_pairs=self.crypto_currency_pairs)

    def test_wrong_constructor_parameter(self):
        with self.assertRaises(Exception):
            AllocationInput({})
