# Radish requirements
colorful==0.5.4
docopt==0.6.2
humanize==0.5.1
parse==1.12.1
parse-type==0.5.2
pysingleton==0.2.1
radish-bdd==0.13.1
six==1.12.0
tag-expressions==1.1.0

# Other requirements
pandas==1.3.4
