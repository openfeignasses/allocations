#!/bin/bash

set -e

# Start in watching mode for production
if [ -n "$KEEP_WATCHING_STRATEGIES_RESULTS" ]; then
    echo "Starting with keep watching strategies_results table mode ..."
    while [ true ]
    do
        python src/main/should_run.py && python src/main/execute.py
        sleep 120 # loop every 2 minutes
    done
fi

# By default start with what is define in CMD
exec "$@"