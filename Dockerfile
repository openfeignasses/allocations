FROM python:3.7

WORKDIR /workspace

COPY ./requirements.txt .

RUN pip install -r requirements.txt

# force cache invalidation if needed
ARG COMMONS_PYTHON_SHASUM
RUN echo $COMMONS_PYTHON_SHASUM
# install commons-python
RUN pip install -e git+https://gitlab.com/openfeignasses/commons-python.git#egg=cryptobot_commons

COPY . .

RUN chmod +x entrypoint.sh
RUN chmod +x wait-for-it.sh

ENTRYPOINT [ "./entrypoint.sh" ]

CMD python src/main/execute.py