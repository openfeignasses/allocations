set -e # exit on fail

if [ ! -d "../commons-python" ]; then
    echo "Error: Directory ../commons-python doesn't exist."
    exit 1
fi
echo "Recompiling code ..."
cd ../commons-python
zip -r cryptobot-commons-latest.zip . -x venv/**\* -x .idea/**\* -x .git/**\*
cd ../allocations
docker build --build-arg=COMMONS_PYTHON_SHASUM="$(sha1sum ../commons-python/cryptobot-commons-latest.zip  | awk '{print $1}')" \
             -t allocations .
echo "Running tests ..."
docker run \
    --rm \
    -v $(pwd)/../commons-python:/commons-python \
    -e PYTHONPATH=/workspace/src/main/ \
    allocations \
    bash -c "pip install /commons-python/cryptobot-commons-latest.zip && python3 -m unittest discover -v -p *_tests.py"
rm ../commons-python/cryptobot-commons-latest.zip