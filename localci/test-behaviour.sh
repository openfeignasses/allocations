set -e # exit on fail

if [ ! -d "../database-migrator" ]; then
    echo "Directory ../database-migrator doesn't exist. Exiting ..."
    exit 1
fi
if [ ! -d "../commons-python" ]; then
    echo "Error: Directory ../commons-python doesn't exist."
    exit 1
fi
echo "Recompiling code ..."
cd ../commons-python
zip -r cryptobot-commons-latest.zip . -x venv/**\* -x .idea/**\* -x .git/**\*
cd ../allocations
docker build --build-arg COMMONS_PYTHON_SHASUM="$(sha1sum ../commons-python/cryptobot-commons-latest.zip  | awk '{print $1}')" \
             -t allocations .
docker build -t database-migrator ../database-migrator
echo "Running tests ..."
running_db_ip=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' mysql)
docker run \
    --rm \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator
docker run \
    --rm \
    --link mysql:mysql \
    -e DATABASE_HOST=mysql \
    -e DATABASE_PORT=3306 \
    -e DATABASE_USER=admin \
    -e DATABASE_PASSWORD=frNH95eSiLYY \
    -e DATABASE_DB=cryptobot \
    database-migrator npm run clear-db
docker run \
    --rm \
    -v $(pwd)/../commons-python:/commons-python \
    --add-host=mysql:$running_db_ip \
    -e MYSQL_DATABASE=cryptobot \
    -e MYSQL_USER=admin \
    -e MYSQL_PASSWORD=frNH95eSiLYY \
    -e DATABASE_HOST=mysql \
    -e PYTHONPATH=/workspace/src/main/ \
    -e DATABASE_PORT="3306" \
    allocations \
    bash -c "cd src/test && radish ./resources/features/ --write-steps-once -t --tags 'not WIP'"
rm ../commons-python/cryptobot-commons-latest.zip